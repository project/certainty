<?php

/**
 * @file
 * Provides plugin definition for Ctools Export UI.
 */

/**
 * Ctools Plugin definition.
 */
$plugin = array(
  // Defined the schema to use for loading objects.
  'schema' => 'certainty_certificate',

  // Define a permission users must have to access these pages.
  'access' => 'administer certainty',

  // Define the menu item.
  'menu' => array(
    'menu prefix' => 'admin/config/system',
    'menu item' => 'certificates',
    'menu title' => 'Certificate Monitoring',
    'menu description' => 'Administer monitoring of public key certificates.',
  ),

  // Define user interface texts.
  'title singular' => t('certificate'),
  'title plural' => t('certificates'),
  'title singular proper' => t('Certainty certificate'),
  'title plural proper' => t('Certainty certificates'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'certainty_ctools_export_ui_form',
    // 'submit' and 'validate' are also valid callbacks.
  ),
);

/**
 * Define the certificate add/edit form.
 */
function certainty_ctools_export_ui_form(&$form, &$form_state) {
  $certificate = $form_state['item'];

  $form['info']['name'] = array_merge($form['info']['name'], array(
    '#title' => t('Machine-readable name of the certificate'),
    '#type' => 'machine_name',
    '#description' => t('The certificate name can only consist of lowercase letters, underscores, and numbers.'),
    '#machine_name' => array(
      'exists' => 'certainty_ctools_export_ui_form_machine_name_exists',
    ),
  ));

  $form['cid'] = array(
    '#type'  => 'value',
    '#value' => isset($certificate->cid) ? $certificate->cid : '',
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Certificate title'),
    '#description' => t('Short phrase identifying this certificate.'),
    '#default_value' => $certificate->title,
    '#required' => TRUE,
  );

  $form['source_type'] = array(
    '#type' => 'select',
    '#title' => t('Source Type'),
    '#description' => t('Source type.'),
    '#options' => array(
      'https' => t('HTTPS URL'),
      'samlsp' => t('SAML Service Provider Metadata URL'),
    ),
    '#default_value' => $certificate->source_type,
  );

  $form['source_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Source Location'),
    '#description' => t('URL or other location.'),
    '#default_value' => $certificate->source_location,
  );

  $form['warning_contacts'] = array(
    '#type' => 'textarea',
    '#title' => t('Warning Contacts'),
    '#description' => t("Example: 'webmaster@example.com' or 'support@example.com,webmaster@example.com' . To specify multiple recipients, separate each e-mail address with a comma."),
    '#default_value' => $certificate->warning_contacts,
  );

  $form['alert_contacts'] = array(
    '#type' => 'textarea',
    '#title' => t('Alert Contacts'),
    '#description' => t("Example: 'info@example.com' or 'ceo@example.com,info@example.com' . To specify multiple recipients, separate each e-mail address with a comma."),
    '#default_value' => $certificate->alert_contacts,
  );

  $form['notes'] = array(
    '#type' => 'textarea',
    '#title' => t('Notes'),
    '#description' => t('Other notes related to this certificate.'),
  );

  return $form;
}

/**
 * Check whether this certificate machine name already exists.
 */
function certainty_ctools_export_ui_form_machine_name_exists($value) {
  $result = db_query('SELECT cid FROM {certainty_certificate} WHERE name = :name', array(':name' => $value))->fetchField();
  return !empty($result);
}
