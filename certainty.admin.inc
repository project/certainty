<?php

/**
 * @file
 * Administrative page callbacks for the Certainty module.
 */

/**
 * Menu callback: displays the search module settings page.
 *
 * @ingroup forms
 *
 * @see certainty_admin_settings_validate()
 * @see certainty_admin_settings_submit()
 */
function certainty_admin_settings($form) {
  $period = drupal_map_assoc(array(
    0, 86400, 172800, 259200, 604800, 1209600, 1814400, 2592000, 5184000, 7776000,
  ), 'format_interval');
  $period[0] = t('<none>');

  $form['certainty_warning_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Warning threshold'),
    '#default_value' => variable_get('certainty_warning_threshold', CERTAINTY_DEFAULT_WARNING_THRESHOLD),
    '#options' => $period,
    '#description' => t('Warning will appear to users with the "view certainty notices" permission.'),
  );

  $form['certainty_alert_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Alert threshold'),
    '#default_value' => variable_get('certainty_alert_threshold', CERTAINTY_DEFAULT_ALERT_THRESHOLD),
    '#options' => $period,
    '#description' => t('Alert will appear to users with the "view certainty notices" permission.'),
  );

  return system_settings_form($form);
}
